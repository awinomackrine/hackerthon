package com.hackerthon.pensionerservice.service;

import com.hackerthon.pensionerservice.dto.PensionerDTO;
import com.hackerthon.pensionerservice.entity.Pensioner;
import com.hackerthon.pensionerservice.model.enums.PensionerStatus;
import com.hackerthon.pensionerservice.repository.PensionerRepository;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class PensionerServiceImplTest {

    private final PensionerServiceImpl pensionerService;

    private final PensionerRepository pensionerRepository;

    PensionerServiceImplTest(PensionerServiceImpl pensionerService, PensionerRepository pensionerRepository) {
        this.pensionerService = pensionerService;
        this.pensionerRepository = pensionerRepository;
    }

    // Saves a new pensioner with all valid fields
    @Test
    public void testSavePensionerDetails_validFields() {
        // Arrange
        PensionerDTO pensionerDTO = new PensionerDTO();
        // Set valid values for all fields in pensionerDTO

        // Act
        Pensioner result = pensionerService.savePensionerDetails(pensionerDTO);

        // Assert
        assertNotNull(result);
        // Add more assertions to verify that the pensioner is saved correctly
    }

    // Returns null when an exception occurs during saving
    @Test
    public void testSavePensionerDetailsExceptionDuringSaving() {
        // Arrange
        PensionerDTO pensionerDTO = new PensionerDTO();
        // Set valid values for all fields in pensionerDTO

        // Mock the pensionerRepository to throw an exception when save method is called

        // Act
        Pensioner result = pensionerService.savePensionerDetails(pensionerDTO);

        // Assert
        assertNull(result);
    }

    // Should return a list of all pensioners when no schemeId is provided
    @Test
    public void testReturnAllPensionersWhenNoSchemeIdProvided() {
        // Arrange
        Long schemeId = null;
        List<Pensioner> expectedPensioners = new ArrayList<>();
        expectedPensioners.add(new Pensioner());
        expectedPensioners.add(new Pensioner());
        when(pensionerRepository.findAll()).thenReturn(expectedPensioners);

        // Act
        List<PensionerDTO> actualPensioners = pensionerService.getAllPensioners(schemeId);

        // Assert
        assertEquals(expectedPensioners.size(), actualPensioners.size());
        assertEquals(expectedPensioners.get(0).getId(), actualPensioners.get(0).getId());
        assertEquals(expectedPensioners.get(1).getId(), actualPensioners.get(1).getId());
    }

    // Should handle null schemeId parameter
    @Test
    public void testHandleNullSchemeIdParameter() {
        // Arrange
        Long schemeId = null;
        List<Pensioner> expectedPensioners = new ArrayList<>();
        expectedPensioners.add(new Pensioner());
        expectedPensioners.add(new Pensioner());
        when(pensionerRepository.findAll()).thenReturn(expectedPensioners);

        // Act
        List<PensionerDTO> actualPensioners = pensionerService.getAllPensioners(schemeId);

        // Assert
        assertEquals(expectedPensioners.size(), actualPensioners.size());
        assertEquals(expectedPensioners.get(0).getId(), actualPensioners.get(0).getId());
        assertEquals(expectedPensioners.get(1).getId(), actualPensioners.get(1).getId());
    }

    // Returns a list of PensionerDTO objects for active and suspended pensioners for a given schemeId
    @Test
    public void testGetPensionersForPayrollActiveAndSuspendedPensioners() {
        // Arrange
        Date pensionDate = new Date();
        Long schemeId = 1L;

        List<Pensioner> pensioners = new ArrayList<>();
        pensioners.add(Pensioner.builder()
                .id(1L)
                .pensionStatus(PensionerStatus.ACTIVE)
                .build());
        pensioners.add(Pensioner.builder()
                .id(2L)
                .pensionStatus(PensionerStatus.SUSPENDED)
                .build());

        when(pensionerRepository.getAllBySchemeId(schemeId)).thenReturn(pensioners);

        PensionerDTO activePensionerDTO = PensionerDTO.builder()
                .id(1L)
                .pensionStatus(PensionerStatus.ACTIVE)
                .build();
        PensionerDTO suspendedPensionerDTO = PensionerDTO.builder()
                .id(2L)
                .pensionStatus(PensionerStatus.SUSPENDED)
                .build();

        List<PensionerDTO> expectedPensioners = new ArrayList<>();
        expectedPensioners.add(activePensionerDTO);
        expectedPensioners.add(suspendedPensionerDTO);

        // Act
        List<PensionerDTO> actualPensioners = pensionerService.getPensionersForPayroll(pensionDate, schemeId);

        // Assert
        assertEquals(expectedPensioners, actualPensioners);
    }

    // Returns an empty list if schemeId is null
    @Test
    public void testGetPensionersForPayroll_NullSchemeId() {
        // Arrange
        Date pensionDate = new Date();
        Long schemeId = null;

        // Act
        List<PensionerDTO> actualPensioners = pensionerService.getPensionersForPayroll(pensionDate, schemeId);

        // Assert
        assertTrue(actualPensioners.isEmpty());
    }
}