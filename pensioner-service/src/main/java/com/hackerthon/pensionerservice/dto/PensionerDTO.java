package com.hackerthon.pensionerservice.dto;

import com.hackerthon.pensionerservice.entity.Pensioner;
import com.hackerthon.pensionerservice.model.enums.Gender;
import com.hackerthon.pensionerservice.model.enums.PayType;
import com.hackerthon.pensionerservice.model.enums.PaymentFrequency;
import com.hackerthon.pensionerservice.model.enums.PensionerStatus;
import com.hackerthon.pensionerservice.model.enums.PensionerType;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for {@link Pensioner}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PensionerDTO {
    private Long id;

    private Date dob;

    private String idNo;

    private String otherNames;

    private String firstName;

    private String surname;

    private String pensionerNo;

    private BigDecimal dcPension;

    private BigDecimal dbPension;

    private Long schemeId;

    private String addressEmail;

    private String addressCellPhone;

    private String addressRoad;

    private String addressPostalAddress;

    private String addressFixedPhone;

    private String addressTown;

    private String pinNumber;

    private PaymentFrequency pensionFrequency;

    private PensionerStatus pensionStatus;

    private PayType payType;

    private PensionerType pensionerType;

    private Date datePrepared;

    private Long preparedById;

    private Date dateCertified;

    private Long certifiedById;

    private Long approvedById;

    private Date dateApproved;

    private Gender gender;
}
