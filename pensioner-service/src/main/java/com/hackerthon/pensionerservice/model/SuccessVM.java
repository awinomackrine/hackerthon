package com.hackerthon.pensionerservice.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor()
@AllArgsConstructor
@ToString()
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class SuccessVM implements Serializable {
    @Ignore
    boolean success = true;

    @Ignore
    long totalCount;

    @Ignore
    Object data;

    @Ignore
    Object rows;

    @Ignore
    String msg;

    @Ignore
    String token;

    public String toJson() {
        try {
            return new ObjectMapper()
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

}
