package com.hackerthon.pensionerservice.model.enums;

import lombok.Getter;

@Getter
public enum PaymentFrequency {
    MONTHLY("Monthly"),
    QUARTERLY("Quarterly"),
    SEMI_ANNUALY("Semi-Annualy"),
    ANNUALY("Annualy");

    private String name;

    PaymentFrequency(String name) {
        this.name = name;
    }

    private PaymentFrequency enumContainsValue(String name) {
        for (PaymentFrequency paymentFrequency : PaymentFrequency.values()) {
            if (paymentFrequency.getName().equalsIgnoreCase(name)) {
                return paymentFrequency;
            }
        }
        return null;
    }
}
