package com.hackerthon.pensionerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PensionerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PensionerServiceApplication.class, args);
	}

}
