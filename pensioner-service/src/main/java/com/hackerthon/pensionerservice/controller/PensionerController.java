package com.hackerthon.pensionerservice.controller;

import com.hackerthon.pensionerservice.dto.PensionerDTO;
import com.hackerthon.pensionerservice.entity.Pensioner;
import com.hackerthon.pensionerservice.service.PensionerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/pensioner")
@RequiredArgsConstructor
@Slf4j
@CrossOrigin(origins = {})
public class PensionerController {

    private final PensionerService pensionerService;

    @PostMapping
    public ResponseEntity<Pensioner> savePensionerDetails(
            @RequestBody PensionerDTO pensionerDTO
    ) {
        log.info("Pensioner details saved: {}", pensionerDTO);
        return new ResponseEntity<>(pensionerService.savePensionerDetails(pensionerDTO), HttpStatus.OK);
    }

    @GetMapping("/{schemeId}")
    public List<PensionerDTO> getAllPensioners(@PathVariable(name = "schemeId") Long schemeId) {

        log.info("Fetching all pensioners for schemeId: {}", schemeId);
        return pensionerService.getAllPensioners(schemeId);
    }

    @GetMapping("/getPensionersForPayroll")
    public List<PensionerDTO> getPensionersForPayroll(
            @RequestParam(name = "pensionDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date pensionDate,
            @RequestParam(name = "schemeId") Long schemeId
    ) {
        log.info("Fetching pensioners for payroll for schemeId: {}", schemeId);
        return pensionerService.getPensionersForPayroll(pensionDate, schemeId);
    }


}
