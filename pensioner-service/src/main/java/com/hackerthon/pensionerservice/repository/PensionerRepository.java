package com.hackerthon.pensionerservice.repository;

import com.hackerthon.pensionerservice.entity.Pensioner;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PensionerRepository extends JpaRepository<Pensioner, Long> {

    List<Pensioner> getAllBySchemeId(Long schemeId);

    List<Pensioner> getAllBySchemeIdAndPensionStartDateBeforeAndDateApprovedNotNullAndPensionStatusIn(Long schemeId, Date asAt, List<String> pensionStatus);

}
