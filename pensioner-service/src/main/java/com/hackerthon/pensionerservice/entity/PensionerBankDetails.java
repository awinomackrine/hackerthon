package com.hackerthon.pensionerservice.entity;

import com.hackerthon.pensionerservice.model.enums.YesNo;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "pensioner_banks")
public class PensionerBankDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Long pensionerId;

    @Column
    private Long bankId;

    @Column
    private Long branchId;

    @Column
    private String accountNo;

    @Column
    private String accountName;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo defaultPoint;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo encrypted;

}
