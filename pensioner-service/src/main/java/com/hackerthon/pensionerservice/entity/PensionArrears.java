package com.hackerthon.pensionerservice.entity;

import com.hackerthon.pensionerservice.model.enums.YesNo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Date;

@Getter
@Setter
//@Entity
//@Table(name = "pension_arrears")
public class PensionArrears implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    //@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "month")
    @Enumerated(EnumType.STRING)
    private Month month;

    @Column(name = "payment_month")
    @Enumerated(EnumType.STRING)
    private Month paymentMonth;

   @Column(name = "date_created")
    private Date dateCreated;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePrepared;

    @Column(name = "writeOff_date")
    private Date writeOffDate;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "year")
    private Integer year;

    @Column(name = "payment_year")
    private Integer paymentYear;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "paid")
    @Enumerated(EnumType.STRING)
    private YesNo paid;

    @Column(name = "written_off")
    @Enumerated(EnumType.STRING)
    private YesNo writtenOff;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo transferredToBens;

    @Column
    private Long transferedFromId;

    @Column(name = "PENSIONER_ID")
    private Long pensionerId;

    @Column(name = "tax")
    private BigDecimal tax;

   /*  @Column(name = "arreasType")
    @Enumerated(EnumType.STRING)
    @Index(name = "paid_idx2")
    private ArrearsType arrearsType;

    @Column(name = "paymode")
    @Enumerated(EnumType.STRING)
    private PaymentModes paymode; */

    @Column(name = "PREPAREDBY_ID")
    private Long preparedById;

    @Column
    private Long refId;

    @Column
    private Long paymentPreparedById;

    @Column
    private Long paymentApprovedById;

    @Column
    @Enumerated
    private YesNo createdByPayroll;

    @Column
    private Long invoiceId;

    @Column
    private Long paymentId;

    @Column
    private Long createdBySuspId; //for merged arrears calculated off system at re-instatement

    @Column
    private Long writtenOffBySuspId;  //updates when the actual arrears are written off thru adjustment. this helps in reversals

    @Column
    private Long paidBySuspId; //updates when the actual arrears are paid without adjustment. this helps in reversals

    @Column
    private Long taxPaidByInvoiceId;


    @Column
    private Long miniPayRollId;

    @Column
    private Long pensionerIdTemp;

    @Column
    private Long createdByPayrollBatchId;

    @Column
    private String createdByPayrollKey;

    @Column
    private String paidByPayrollKey;

    @Column
    private Long createdByBatchRevisionId;

    @Column
    private Long accrualInvoiceId;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaymentPrepared;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaymentApproved;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo certified;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo approved;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo taxed;

    @Column
    private Long certifiedById;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaymentCertified;

    @Column
    private Long approvedById;




}
