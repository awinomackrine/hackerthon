package com.hackerthon.pensionerservice.entity;


import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Address implements Serializable {

    @Column
    private String email;

    @Column
    private String cellPhone;

    @Column
    private String road;

    @Column
    private String postalAddress;

    @Column
    private String fixedPhone;

    @Column
    private String town;
}
