package com.hackerthon.pensionerservice.entity;

import com.hackerthon.pensionerservice.model.enums.Gender;
import com.hackerthon.pensionerservice.model.enums.PayType;
import com.hackerthon.pensionerservice.model.enums.PaymentFrequency;
import com.hackerthon.pensionerservice.model.enums.PensionerStatus;
import com.hackerthon.pensionerservice.model.enums.PensionerType;
import com.hackerthon.pensionerservice.model.enums.YesNo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "pensioners")
public class Pensioner implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @Temporal(TemporalType.DATE)
    private Date dob;

    @Column
    private String idNo;

    @Column
    private String otherNames;

    @Column
    private String firstName;

    @Column
    private String surname;

    @Column
    private String pensionerNo;

    @Column
    private String externalPensionerNo;

    @Column
    private String socialSecurityNo;

    @Column
    private BigDecimal dcPension;

    @Column
    private BigDecimal dbPension;

    @Column(name = "member_Id")
    private Long memberId;

    @Column(name = "beneficiary_Id")
    private Long beneficiaryId;

    @Column(name = "scheme_Id")
    private Long schemeId;

    @Embedded
    private Address address;

    @Column
    private String partnerNumber;

    @Column
    private String pinNumber;

    @Column
    @Enumerated(EnumType.STRING)
    private PaymentFrequency pensionFrequency;

    @Column
    @Enumerated(EnumType.STRING)
    private PensionerStatus pensionStatus;

    @Column
    private BigDecimal purchasePrice;

    @Column
    private BigDecimal increaseInDcPension;

    @Column
    private BigDecimal increaseInDbPension;


    @Column
    private Integer guaranteedPeriod;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date selfPensionerStartDate;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusChangeDate;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date pensionStartDate;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date pensionStopDate;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRevision;

    @Column
    @Enumerated(EnumType.STRING)
    private PayType payType;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo alive;

    @Column
    private String formerScheme;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo hasOtherIncome;

    @Column
    @Enumerated(EnumType.STRING)
    private PensionerType pensionerType;

    @Column
    @Enumerated(EnumType.STRING)
    private PensionerStatus prevStatus;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo deleted;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePrepared;

    @Column(name = "preparedBy_id")
    private Long preparedById;

    @Column(name = "batch_Id")
    private Long batchId;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCertified;

    @Column(name = "certifiedBy_id")
    private Long certifiedById;

    @Column(name = "approvedBy_id")
    private Long approvedById;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateApproved;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo guaranteedPeriodElapsed;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo guaranteedPeriodSmsSent;

    @Column
    @Temporal(TemporalType.DATE)
    private Date guaranteedPeriodEndDate;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo hasDisability;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo subjectToTax;

    @Column
    @Enumerated(EnumType.STRING)
    private YesNo employerPMSEligibility;

    @Column
    @Temporal(TemporalType.DATE)
    private Date disabilityStatusDate;

    @Column
    private String disabilityRemarks;

    @Column
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column
    private BigDecimal previousDcPension;

    @Column
    private BigDecimal previousDbPension;

}
