package com.hackerthon.pensionerservice.service;

import com.hackerthon.pensionerservice.dto.PensionerDTO;
import com.hackerthon.pensionerservice.entity.Pensioner;

import java.util.Date;
import java.util.List;

public interface PensionerService {
    Pensioner savePensionerDetails(PensionerDTO pensionerDTO);

    List<PensionerDTO> getAllPensioners(Long schemeId);

    List<PensionerDTO> getPensionersForPayroll(Date pensionDate, Long schemeId);
}
