package com.hackerthon.pensionerservice.service;

import com.hackerthon.pensionerservice.dto.PensionerDTO;
import com.hackerthon.pensionerservice.entity.Address;
import com.hackerthon.pensionerservice.entity.Pensioner;
import com.hackerthon.pensionerservice.model.SuccessVM;
import com.hackerthon.pensionerservice.model.enums.PensionerStatus;
import com.hackerthon.pensionerservice.repository.PensionerRepository;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class PensionerServiceImpl implements PensionerService {

    private final PensionerRepository pensionerRepository;

    @Override
    public Pensioner savePensionerDetails(PensionerDTO pensionerDTO) {

        try {
            Pensioner pensioner;

            /* if (pensionerDTO == null) {
                return SuccessVM.builder()
                        .success(false)
                        .msg("Pensioner body cannot be empty")
                        .build();
            } */

            log.info("Pensioner details saved: {}", pensionerDTO);

            if (pensionerDTO.getId() != null) {
                pensioner = pensionerRepository.findById(pensionerDTO.getId()).orElseGet(Pensioner::new);
            }

            Address address = Address.builder()
                    .email(pensionerDTO.getAddressEmail())
                    .cellPhone(pensionerDTO.getAddressCellPhone())
                    .road(pensionerDTO.getAddressRoad())
                    .postalAddress(pensionerDTO.getAddressPostalAddress())
                    .fixedPhone(pensionerDTO.getAddressFixedPhone())
                    .town(pensionerDTO.getAddressTown())
                    .build();

            pensioner = Pensioner.builder()
                    .dob(pensionerDTO.getDob())
                    .idNo(pensionerDTO.getIdNo())
                    .otherNames(pensionerDTO.getOtherNames())
                    .firstName(pensionerDTO.getFirstName())
                    .surname(pensionerDTO.getSurname())
                    .pensionerNo(pensionerDTO.getPensionerNo())
                    .dcPension(pensionerDTO.getDcPension())
                    .dbPension(pensionerDTO.getDbPension())
                    .schemeId(pensionerDTO.getSchemeId())
                    .address(address)
                    .pinNumber(pensionerDTO.getPinNumber())
                    .pensionFrequency(pensionerDTO.getPensionFrequency())
                    .pensionStatus(pensionerDTO.getPensionStatus())
                    .pensionerType(pensionerDTO.getPensionerType())
                    .payType(pensionerDTO.getPayType())
                    .gender(pensionerDTO.getGender())
                    .build();

            pensioner = pensionerRepository.save(pensioner);

            return pensioner;


            /* return SuccessVM.builder()
                    .success(true)
                    .msg("Pensioner details saved successfully")
                    .data(pensioner)
                    .build(); */

        } catch (Exception e) {
            log.error("Error occurred while saving pensioner details: {}", e.getMessage());
            return null;
        }

    }

    @Override
    public List<PensionerDTO> getAllPensioners(Long schemeId) {
        try {

            List<Pensioner> pensioners = schemeId != null
                    ? pensionerRepository.getAllBySchemeId(schemeId)
                    : pensionerRepository.findAll();

            return pensioners.stream()
                    .map(this::mapPensionerToPensionerDTO)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            log.error("Error occurred while fetching pensioners: {}", e.getMessage());
            return Collections.emptyList();

        }
    }

    @Override
    public List<PensionerDTO> getPensionersForPayroll(Date pensionDate, Long schemeId) {
        try {

            List<String> pensionStatus = List.of(
                    PensionerStatus.ACTIVE.name(),

                    PensionerStatus.SUSPENDED.name()
            );

            List<Pensioner> pensioners = pensionerRepository.getAllBySchemeId(schemeId);

            return pensioners.stream()
                    .map(this::mapPensionerToPensionerDTO)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            log.error("Error occurred while fetching pensioners for payroll: {}", e.getMessage());
            return Collections.emptyList();

        }

    }

    private PensionerDTO mapPensionerToPensionerDTO(Pensioner pensioner) {
        return PensionerDTO.builder()
                .id(pensioner.getId())
                .dob(pensioner.getDob())
                .idNo(pensioner.getIdNo())
                .otherNames(pensioner.getOtherNames())
                .firstName(pensioner.getFirstName())
                .surname(pensioner.getSurname())
                .pensionerNo(pensioner.getPensionerNo())
                .dcPension(pensioner.getDcPension())
                .dbPension(pensioner.getDbPension())
                .schemeId(pensioner.getSchemeId())
                .addressEmail(pensioner.getAddress().getEmail())
                .addressCellPhone(pensioner.getAddress().getCellPhone())
                .addressRoad(pensioner.getAddress().getRoad())
                .addressPostalAddress(pensioner.getAddress().getPostalAddress())
                .addressFixedPhone(pensioner.getAddress().getFixedPhone())
                .addressTown(pensioner.getAddress().getTown())
                .pinNumber(pensioner.getPinNumber())
                .pensionFrequency(pensioner.getPensionFrequency())
                .pensionStatus(pensioner.getPensionStatus())
                .payType(pensioner.getPayType())
                .pensionerType(pensioner.getPensionerType())
                .datePrepared(pensioner.getDatePrepared())
                .preparedById(pensioner.getPreparedById())
                .dateCertified(pensioner.getDateCertified())
                .certifiedById(pensioner.getCertifiedById())
                .approvedById(pensioner.getApprovedById())
                .dateApproved(pensioner.getDateApproved())
                .build();
    }
}
