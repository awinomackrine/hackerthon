package com.hackerthon.payrollservice.service;

import com.hackerthon.payrollservice.dto.PayrollBatchDto;
import com.hackerthon.payrollservice.entity.PayrollBatch;
import com.hackerthon.payrollservice.repository.PayrollBatchRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import com.hackerthon.payrollservice.model.enums.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PayrollBatchServiceImplTest {
    // Returns a list of PayrollBatchDto when called with a valid schemeId.
    // Returns a list of PayrollBatchDto objects when given a valid schemeId.

    private final PayrollBatchRepository payrollBatchRepository;

    PayrollBatchServiceImplTest(PayrollBatchRepository payrollBatchRepository) {
        this.payrollBatchRepository = payrollBatchRepository;
    }

    @Test
    public void testValidSchemeId() {
        // Arrange
        Long schemeId = 1L;
        List<PayrollBatch> payrollBatches = new ArrayList<>();
        payrollBatches.add(PayrollBatch.builder()
                .id(1L)
                .month(Month.JAN)
                .year(2022)
                .netTotal(BigDecimal.valueOf(1000))
                .batchCount(10)
                .build());
        payrollBatches.add(PayrollBatch.builder()
                .id(2L)
                .month(Month.FEB)
                .year(2022)
                .netTotal(BigDecimal.valueOf(2000))
                .batchCount(20)
                .build());
        Mockito.when(payrollBatchRepository.getAllBySchemeId(schemeId)).thenReturn(payrollBatches);

        PayrollBatchServiceImpl payrollBatchService = new PayrollBatchServiceImpl(payrollBatchRepository);

        // Act
        List<PayrollBatchDto> result = payrollBatchService.getAllPayrollBatches(schemeId);

        // Assert
        assertEquals(2, result.size());
        assertEquals(1L, result.get(0).getId().longValue());
        assertEquals(Month.JAN, result.get(0).getMonth());
        assertEquals(2022, result.get(0).getYear());
        assertEquals(BigDecimal.valueOf(1000), result.get(0).getNetTotal());
        assertEquals(10, result.get(0).getBatchCount().intValue());
        assertEquals(2L, result.get(1).getId().longValue());
        assertEquals(Month.FEB, result.get(1).getMonth());
        assertEquals(2022, result.get(1).getYear());
        assertEquals(BigDecimal.valueOf(2000), result.get(1).getNetTotal());
        assertEquals(20, result.get(1).getBatchCount().intValue());
    }
}