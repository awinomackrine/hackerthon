package com.hackerthon.payrollservice.service;

import com.hackerthon.payrollservice.model.RunPayrollRequest;
import com.hackerthon.payrollservice.repository.PayrollBatchRepository;
import com.hackerthon.payrollservice.repository.PayrollRepository;
import org.junit.jupiter.api.Test;
import com.hackerthon.payrollservice.model.enums.Month;
import org.reactivestreams.Publisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;


import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static reactor.core.publisher.Mono.when;

class PayrollServiceImplTest {

    // The method should return a ResponseEntity with status code 200 and message "Scheme Ids cannot be empty" if schemeId is null
    @Test
    public void testSchemeIdNull() {
        // Arrange
        RunPayrollRequest runPayrollRequest = new RunPayrollRequest();
        runPayrollRequest.setSchemeId(null);
        runPayrollRequest.setMonth(Month.JAN);
        runPayrollRequest.setYear(2022);
        runPayrollRequest.setDatePrepared(new Date());

        // Act
        PayrollServiceImpl payrollServiceImpl = new PayrollServiceImpl();
        ResponseEntity<String> response = payrollServiceImpl.runPayroll(runPayrollRequest);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Scheme Ids cannot be empty", response.getBody());
    }

    // The method should handle gracefully if an exception is thrown while calling the external service to get pensioners for payroll

}