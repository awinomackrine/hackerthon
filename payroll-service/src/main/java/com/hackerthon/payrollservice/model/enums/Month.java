package com.hackerthon.payrollservice.model.enums;

import lombok.Getter;

@Getter
public enum Month {
    JAN("January", 1),
    FEB("February", 2),
    MAR("March", 3),
    APR("April", 4),
    MAY("May", 5),
    JUN("June", 6),
    JUL("July", 7),
    AUG("August", 8),
    SEP("September", 9),
    OCT("October", 10),
    NOV("November", 11),
    DEC("December", 12);

    private String name;
    private final int index;

    Month(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public static Month get(String name) {
        if (name == null || name.isEmpty()) return null;
        for (Month value : Month.values()) {
            if (value.name().equalsIgnoreCase(name) || value.getName().equalsIgnoreCase(name)) return value;
        }
        return null;
    }

    public static Month fromOrd(int i) {
        if (i < 0 || i >= Month.values().length) {
            throw new IndexOutOfBoundsException("Invalid ordinal");
        }
        return Month.values()[i];
    }

    public static Month fromString(String name) {
        for (Month b : Month.values()) {
            if (b.getName().equalsIgnoreCase(name) || b.name().equalsIgnoreCase(name)) {
                return b;
            }
        }
        return null;
    }
}
