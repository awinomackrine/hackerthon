package com.hackerthon.payrollservice.model;

import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.binary.Base64;

public class EncryptDecrypt {

    private final static String myEncryptionKey = "INZUKHAAHKUZNIINZUKHA#2018";

    private final static String myEncryptionScheme = "DESede";

    private final static String UNICODE_FORMAT = "UTF8";

    private final static Integer dateEncryptionId = 1460302; //(4000*365)+(8*30)+(31*2)

    public static String encrypt(String plain) {
        String encryptedString = null;
        try {
            Cipher cipher = Cipher.getInstance(myEncryptionScheme);
            cipher.init(Cipher.ENCRYPT_MODE, getKey());
            byte[] plainText = plain.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString =
                    new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return encryptedString;
    }

    public static String decrypt(String encryptedString) {
        String decryptedText = null;
        try {
            Cipher cipher = Cipher.getInstance(myEncryptionScheme);
            cipher.init(Cipher.DECRYPT_MODE, getKey());
            byte[] encryptedText = Base64.decodeBase64(encryptedString.getBytes());
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText = new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return decryptedText;
    }

    private static SecretKey getKey() throws Exception {
        byte[] arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        KeySpec ks = new DESedeKeySpec(arrayBytes);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        Cipher cipher = Cipher.getInstance(myEncryptionScheme);
        return skf.generateSecret(ks);
    }


}
