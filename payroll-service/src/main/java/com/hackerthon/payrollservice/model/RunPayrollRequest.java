package com.hackerthon.payrollservice.model;

import com.hackerthon.payrollservice.model.enums.Month;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RunPayrollRequest {
    private Long schemeId;

    private Month month;

    private int year;

    private Date datePrepared;
}
