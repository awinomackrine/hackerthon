package com.hackerthon.payrollservice.model.enums;

public enum Gender {
    MALE("Male"),
    FEMALE("Female"),
    NOT_APPLICABLE("Not Applicable"),
    UNKNOWN("Unknown");

    private String name;

    Gender(String name) {
        this.name = name;
    }

    public static Gender from(String gender) {
        if (gender != null) {
            for (Gender b : Gender.values()) {
                if (gender.equalsIgnoreCase(b.name) || gender.equalsIgnoreCase(b.name())) {
                    return b;
                }
            }
        }
        return null;
    }

    public static Gender fromString(String gender) {
        if (gender != null) {
            for (Gender b : Gender.values()) {
                if (gender.equalsIgnoreCase(b.name) || gender.equalsIgnoreCase(b.name())) {
                    return b;
                }
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
