package com.hackerthon.payrollservice.model.enums;

public enum PensionerStatus {

    ACTIVE("Active"),
    SUSPENDED("Suspended"),
    DEFFERED("Deffered"),
    STOPPED("Stopped"),
    DECEASED("Deceased"),
    DELETED("Deleted"),
    NOTICE_OF_EXIT("On Notice Of Exit"),
    COMMUTED("Commuted"),
    STOPPED_AND_UNPAID_PENSION_CAPITALIZED("Stopped and Unpaid Pension Capitalized"),
    RETIRED_AND_TRIVIAL_PENSION_PAID("Retired and Trivial Pension Paid");

    private String name;

    private PensionerStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PensionerStatus fromOrdinal(int ord) {
        for (PensionerStatus b : PensionerStatus.values()) {
            if (b.ordinal() == ord) {
                return b;
            }
        }
        return null;
    }

    public static PensionerStatus fromString(String text) {
        if (text != null) {
            for (PensionerStatus state : PensionerStatus.values()) {
                if (text.equalsIgnoreCase(state.name()) || text.equalsIgnoreCase(state.toString())) {
                    return state;
                }
            }
        }
        return null;
    }

}
