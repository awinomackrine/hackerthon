package com.hackerthon.payrollservice.model.enums;

import lombok.Getter;

@Getter
public enum PensionerType {
    WIDOW_WIDOWER("Widow(er)"),
    ORPHAN("Orphan"),
    SELF("Self"),
    SUCCESSION_ESTATE("Succession Estate"),
    PARENT("Parent"),
    POTENTIAL_ANNUITANT("Potential Annuitant"),
    DRAWDOWN("Drawdown");

    private String name;

    PensionerType(String name) {
        this.name = name;
    }

    public static PensionerType fromString(String name) {
        for (PensionerType b : PensionerType.values()) {
            if (b.getName().equalsIgnoreCase(name) || b.name().equalsIgnoreCase(name)) {
                return b;
            }
        }
        return null;
    }
}
