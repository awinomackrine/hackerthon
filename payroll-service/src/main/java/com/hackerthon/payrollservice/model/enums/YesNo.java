package com.hackerthon.payrollservice.model.enums;

/**
 * @author kodero
 */
public enum YesNo {
  YES("Yes"),
  NO("No"),
  N_A("N/A");

  private String name;

  YesNo(String name) {
    this.name = name;
  }

  public static YesNo get(String val) {
    YesNo yesNo = fromString(val);
    if (yesNo != null) return yesNo;
    return YesNo.NO;
  }

  public static YesNo get(YesNo val1, YesNo val2) {
    val1 = val1 != null ? val1 : YesNo.NO;
    val2 = val2 != null ? val2 : YesNo.NO;
    return get(val1.name()).equals(get(val2.name())) ? val1 : YesNo.NO;
  }

  public static YesNo fromString(String val) {
    if (val == null || val.isEmpty()) return null;
    val = val.trim();
    if (val.equalsIgnoreCase("YES")) return YES;
    if (val.equalsIgnoreCase("NO")) return NO;
    return null;
  }

  public static YesNo fromOrdinal(int ord) {
    for (YesNo b : YesNo.values()) {
      if (b.ordinal() == ord) {
        return b;
      }
    }
    return null;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}
