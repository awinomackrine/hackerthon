package com.hackerthon.payrollservice.model.enums;

import lombok.Getter;

@Getter
public enum PayType {

    BANK("Bank"),
    FINANCIAL_INSTITUTION("Financial Institution"),
    PAYPOINT("Paypoint"),
    DIRECT_CHEQUES("Direct Cheque"),
    MOBILE_MONEY("Mobile Money");

    private String name;

    PayType(String name) {
        this.name = name;
    }

    private PayType enumContainsValue(String name) {
        for (PayType payType : PayType.values()) {
            if (payType.getName().equalsIgnoreCase(name)) {
                return payType;
            }
        }
        return null;
    }
}
