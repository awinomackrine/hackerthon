package com.hackerthon.payrollservice.service;

import com.hackerthon.payrollservice.dto.PensionerForPayrollDTO;
import com.hackerthon.payrollservice.model.RunPayrollRequest;
import com.hackerthon.payrollservice.model.enums.Month;
import java.util.Date;
import java.util.List;
import org.springframework.http.ResponseEntity;

public interface PayrollService {
    ResponseEntity<String> runPayroll(RunPayrollRequest runPayrollRequest);

    ResponseEntity<String> processMonthlyPayroll(List<PensionerForPayrollDTO> pensionerDTOList, Month month, int year, Date payrollDate, Long schemeId);
}
