package com.hackerthon.payrollservice.service;

import com.hackerthon.payrollservice.dto.PayrollBatchDto;

import java.util.List;

public interface PayrollBatchService {
    List<PayrollBatchDto> getAllPayrollBatches(Long schemeId);
}
