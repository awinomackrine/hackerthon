package com.hackerthon.payrollservice.service;

import com.hackerthon.payrollservice.dto.PensionerForPayrollDTO;
import com.hackerthon.payrollservice.entity.Payroll;
import com.hackerthon.payrollservice.entity.PayrollBatch;
import com.hackerthon.payrollservice.model.EncryptDecrypt;
import com.hackerthon.payrollservice.model.RunPayrollRequest;
import com.hackerthon.payrollservice.model.enums.Month;
import com.hackerthon.payrollservice.model.enums.PensionerStatus;
import com.hackerthon.payrollservice.repository.PayrollBatchRepository;
import com.hackerthon.payrollservice.repository.PayrollRepository;
import com.hackerthon.payrollservice.util.DateUtil;
import org.joda.time.DateTime;
import org.springframework.http.HttpStatusCode;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class PayrollServiceImpl implements PayrollService {

    private final PayrollBatchRepository payrollBatchRepository;

    private final PayrollRepository payrollRepository;

    private WebClient.Builder webClientBuilder;

    public PayrollServiceImpl(
            PayrollBatchRepository payrollBatchRepository,
            PayrollRepository payrollRepository,
            WebClient.Builder webClientBuilder
    ) {
        this.payrollBatchRepository = payrollBatchRepository;
        this.payrollRepository = payrollRepository;
        this.webClientBuilder = webClientBuilder;
    }


    @Override
    public ResponseEntity<String> runPayroll(RunPayrollRequest runPayrollRequest) {
        Long schemeId = runPayrollRequest.getSchemeId();

        Month month = runPayrollRequest.getMonth();

        int year = runPayrollRequest.getYear();

        Date datePrepared = runPayrollRequest.getDatePrepared();

        if (schemeId == null) {
            return ResponseEntity.ok("Scheme Ids cannot be empty");
        }

        if (month == null) {
            return ResponseEntity.ok("Month cannot be empty");
        }

        if (year == 0) {
            return ResponseEntity.ok("Year cannot be empty");
        }

        if (datePrepared == null) {
            return ResponseEntity.ok("Date Prepared cannot be empty");
        }

        PayrollBatch payrollBatch = payrollBatchRepository.getPayrollBatchByMonthAndYearAndSchemeId(month, year, schemeId);

        if (payrollBatch != null) {
            return ResponseEntity.ok("Payroll already exists for the month and year");
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        PensionerForPayrollDTO[] payrollPensioners = webClientBuilder.build().get()
                .uri("http://pensioner-service/api/pensioner/getPensionersForPayroll",
                        uriBuilder -> uriBuilder
                                .queryParam("pensionDate", dateFormat.format(datePrepared))
                                .queryParam("schemeId", schemeId)
                                .build())
                .retrieve()
                .onStatus(HttpStatusCode::isError, clientResponse -> handleError(clientResponse))
                .bodyToMono(PensionerForPayrollDTO[].class)
                .block();

        log.info("{}", payrollPensioners);

        if (payrollPensioners == null || payrollPensioners.length == 0) {
            return ResponseEntity.badRequest().body("No pensioners found for the scheme");
        }

        List<PensionerForPayrollDTO> pensionerDTOList = List.of(payrollPensioners);

        return this.processMonthlyPayroll(pensionerDTOList, month, year, datePrepared, schemeId);

    }

    @Override
    public ResponseEntity<String> processMonthlyPayroll(List<PensionerForPayrollDTO> pensionerDTOList, Month month, int year, Date payrollDate, Long schemeId) {

        try {
            String  batchKey = EncryptDecrypt.encrypt(month.getName()+"/"+year+"/"+schemeId+"/"+new DateTime());

            PayrollBatch payrollBatch = PayrollBatch.builder()
                    .month(month)
                    .year(year)
                    .batchCount(0)
                    .grossTotal(BigDecimal.ZERO)
                    .netTotal(BigDecimal.ZERO)
                    .schemeId(schemeId)
                    .batchKey(batchKey)
                    .datePrepared(payrollDate)
                    .build();

            List<Payroll> payrollsForTheMonth = new ArrayList<>();

            AtomicInteger counter = new AtomicInteger();


            pensionerDTOList.parallelStream().forEach(pensionerDTO -> {

                Payroll payroll = this.processPensionerForPayroll(pensionerDTO, month, year, payrollDate, payrollBatch);

                if (PensionerStatus.ACTIVE.equals(pensionerDTO.getPensionStatus())) {
                    counter.getAndIncrement();

                    payrollsForTheMonth.add(payroll);
                    payrollBatch.setNetTotal(payrollBatch.getNetTotal().add(payroll.getNet() == null ? BigDecimal.ZERO : payroll.getNet()));
                    payrollBatch.setGrossTotal(payrollBatch.getGrossTotal().add(payroll.getGross() == null ? BigDecimal.ZERO : payroll.getGross()));

                }

            });

            payrollBatch.setPayrolls(payrollsForTheMonth);

            payrollBatch.setBatchCount(counter.get());

            payrollBatchRepository.save(payrollBatch);

            return ResponseEntity.ok("Payroll processed successfully");

        } catch (Exception e) {
            log.error("Error occurred while processing payroll: {}", e.getMessage());
            return ResponseEntity.badRequest().body("Error occurred while processing payroll");
        }


    }

    private Payroll processPensionerForPayroll(PensionerForPayrollDTO pensionerDTO, Month month, int year, Date payrollDate,
                                               PayrollBatch payrollBatch) {
        //Include Check For Proration
        BigDecimal grossMonthlyPension = this.calculateGrossMonthlyPension(pensionerDTO, month, year, payrollDate);

        return Payroll.builder()
                .pensionerId(pensionerDTO.getId())
                .gross(grossMonthlyPension)
                .net(grossMonthlyPension)
                .month(month)
                .year(year)
                .batch(payrollBatch)
                .build();
    }

    private BigDecimal calculateGrossMonthlyPension(PensionerForPayrollDTO pensionerDTO, Month month, int year, Date payrollDate) {
        Calendar cal = Calendar.getInstance();

        Date stopDate = pensionerDTO.getPensionStopDate();

        // Calculate the total monthly pension
        BigDecimal dcPension = pensionerDTO.getDcPension() == null ? BigDecimal.ZERO : pensionerDTO.getDcPension();
        BigDecimal dbPension = pensionerDTO.getDbPension() == null ? BigDecimal.ZERO : pensionerDTO.getDbPension();

        BigDecimal totalPension = dcPension.add(dbPension);

        if (stopDate != null) {
            // Set the given month and year
            cal.set(Calendar.MONTH, month.ordinal());
            cal.set(Calendar.YEAR, year);

            // Get number of days in the given month and year
            int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

            // Get the number of days elapsed in the given month before  date
            int daysElapsed = DateUtil.getDaysElapsedInMonth(stopDate, year, month.ordinal() + 1);

            return (totalPension.divide(BigDecimal.valueOf(daysInMonth), 2, RoundingMode.UP)).multiply(BigDecimal.valueOf(daysElapsed));

        }

        return totalPension;
    }

    private Mono<? extends Throwable> handleError(ClientResponse response) {
        return Mono.error(new RuntimeException("Error occurred while fetching pensioners"));
    }
}
