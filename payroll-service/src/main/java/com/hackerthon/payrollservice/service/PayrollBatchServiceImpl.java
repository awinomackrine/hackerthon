package com.hackerthon.payrollservice.service;

import com.hackerthon.payrollservice.dto.PayrollBatchDto;
import com.hackerthon.payrollservice.dto.PayrollDto;
import com.hackerthon.payrollservice.entity.PayrollBatch;
import com.hackerthon.payrollservice.repository.PayrollBatchRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PayrollBatchServiceImpl implements PayrollBatchService{

    private final PayrollBatchRepository payrollBatchRepository;

    @Override
    public List<PayrollBatchDto> getAllPayrollBatches(Long schemeId) {

        List<PayrollBatch> payrollBatches = schemeId != null
                ? payrollBatchRepository.getAllBySchemeId(schemeId)
                : payrollBatchRepository.findAll();

        return payrollBatches.stream()
                .map(this::mapToPayrollBatchDto)
                .collect(Collectors.toList());

    }

    private PayrollBatchDto mapToPayrollBatchDto(PayrollBatch payrollBatch) {
        return PayrollBatchDto.builder()
                .id(payrollBatch.getId())
                .month(payrollBatch.getMonth())
                .year(payrollBatch.getYear())
                .netTotal(payrollBatch.getNetTotal())
                .batchCount(payrollBatch.getBatchCount())
                .build();
    }
}

