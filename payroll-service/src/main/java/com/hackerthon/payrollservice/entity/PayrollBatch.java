package com.hackerthon.payrollservice.entity;

import com.hackerthon.payrollservice.entity.Payroll;
import com.hackerthon.payrollservice.model.enums.Month;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "payroll_batches")
public class PayrollBatch implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @Enumerated(EnumType.STRING)
    private Month month;

    @Column
    private int year;

    @Column
    private String batchKey;

    @Column
    private Long preparedById;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePrepared;

    @Column
    private Integer batchCount;

    @Column
    private BigDecimal tax;

    @Column
    private BigDecimal ded;

    @Column
    private BigDecimal netTotal;

    @Column
    private BigDecimal grossTotal;

    @Column
    private Long schemeId;

    @OneToMany(mappedBy = "batch", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Payroll> payrolls = new ArrayList<Payroll>();


    public synchronized void addPayroll(Payroll payroll) {
        payroll.setBatch(this);
        getPayrolls().add(payroll);
    }

}
