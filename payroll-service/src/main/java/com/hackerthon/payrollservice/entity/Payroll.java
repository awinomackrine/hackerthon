package com.hackerthon.payrollservice.entity;

import com.hackerthon.payrollservice.model.enums.Month;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "payrolls")
public class Payroll implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @Enumerated(EnumType.STRING)
    private Month month;

    @Column
    private int year;

    @Column
    private BigDecimal gross;

    @Column
    private BigDecimal net;

    @Column
    private BigDecimal tax;

    @Column
    private BigDecimal deduction;

    @Column
    private BigDecimal arrears;

    @ManyToOne(cascade = CascadeType.ALL)
    private PayrollBatch batch;

    @Column
    private Long pensionerId;

    //Payment details

    @Column
    private String accountNo;

    @Column
    private String accountName;

    @Column
    private Long branchId;

    @Column
    private Long bankId;


    @Column
    private Long certifiedBy;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date certifiedDate;

    @Column
    private Long approvedBy;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedDate;
}
