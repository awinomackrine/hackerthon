package com.hackerthon.payrollservice.controller;

import com.hackerthon.payrollservice.dto.PayrollBatchDto;
import com.hackerthon.payrollservice.dto.PayrollDto;
import com.hackerthon.payrollservice.model.RunPayrollRequest;
import com.hackerthon.payrollservice.service.PayrollBatchService;
import com.hackerthon.payrollservice.service.PayrollService;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/payroll")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:1841", allowCredentials = "true")
public class PayrollController {

    private final PayrollService payrollService;

    private final PayrollBatchService payrollBatchService;

    @PostMapping("runPayroll")
    public ResponseEntity<String> runPayroll(
            @RequestBody RunPayrollRequest runPayrollRequest
    ) {

        return payrollService.runPayroll(runPayrollRequest);
    }


    @GetMapping("/{schemeId}")
    public List<PayrollBatchDto> getAllPayrollBatches(@PathVariable(name = "schemeId")  Long schemeId) {

        return payrollBatchService.getAllPayrollBatches(schemeId);
    }

}
