package com.hackerthon.payrollservice.util;

import com.hackerthon.payrollservice.model.enums.Month;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.joda.time.DateTime;

public class DateUtil implements java.io.Serializable {
  public static double DifferenceInMonths(Date date1, Date date2) {
    return DifferenceInYears(date1, date2) * 12;
  }

  public static double DifferenceInYears(Date date1, Date date2) {
    double days = DifferenceInDays(date1, date2);
    return days / 365.2425;
  }

  public static double DifferenceInDays(Date date1, Date date2) {
    return DifferenceInHours(date1, date2) / 24.0;
  }

  public static double DifferenceInHours(Date date1, Date date2) {
    return DifferenceInMinutes(date1, date2) / 60.0;
  }

  public static double DifferenceInMinutes(Date date1, Date date2) {
    return DifferenceInSeconds(date1, date2) / 60.0;
  }

  public static double DifferenceInSeconds(Date date1, Date date2) {
    return DifferenceInMilliseconds(date1, date2) / 1000.0;
  }

  private static double DifferenceInMilliseconds(Date date1, Date date2) {
    return Math.abs(GetTimeInMilliseconds(date1) - GetTimeInMilliseconds(date2));
  }

  private static long GetTimeInMilliseconds(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return cal.getTimeInMillis() + cal.getTimeZone().getOffset(cal.getTimeInMillis());
  }

  public static String formatDateUsingLocale(Date date) {
    if (date == null) {
      return "";
    }
    if (Locale.getDefault().equals(Locale.US)) {
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
      return asLocalDate(date).format(formatter);

    }else{
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
      return asLocalDate(date).format(formatter);
    }

  }

    public static String formatDateToGrid(Date date) {
        if (date == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        return asLocalDate(date).format(formatter);

    }

    public static String formatDateWithFullMonthName(Date date) {
        if (date == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        return asLocalDate(date).format(formatter);

    }

    public static String formatDateAndTimeToGrid(Date date) {
        if (date == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");
        return asLocalDate(date).format(formatter);

    }

    public static String formatDateUsingLocale(LocalDate date) {
        if (date == null) {
            return "";
        }
        if (Locale.getDefault().equals(Locale.US)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            return date.format(formatter);

        }else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            return date.format(formatter);
        }

    }

    public static Date formatStringToDateUsingLocale(String dateString) {

        if (dateString == null) {
            return null;
        }

        try{

            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

            if (Locale.getDefault().equals(Locale.UK)) {
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            }

            return dateFormat.parse(dateString);

        }catch (Exception e){
            return null;
        }

    }

  public static LocalDate asLocalDate(Date date) {
    return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
  }

  public static Date formatMssDateStringToDate(String dateString){
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    //parse date separately incase of error or null dates
    try {
      if(dateString != null) {
          return dateFormat.parse(dateString);
      }

    } catch (ParseException ignored) {

    }
      return null;
  }


    public static int getDaysInMonth(int year, int month) {
        int daysInMonth;
        switch (month) {
            case 1: // January
            case 3: // March
            case 5: // May
            case 7: // July
            case 8: // August
            case 10: // October
            case 12: // December
                daysInMonth = 31;
                break;
            case 4: // April
            case 6: // June
            case 9: // September
            case 11: // November
                daysInMonth = 30;
                break;
            case 2: // February
                if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                    daysInMonth = 29;
                } else {
                    daysInMonth = 28;
                }
                break;
            default:
                daysInMonth = 0; // invalid month
        }
        return daysInMonth;
    }

    public static int getDaysInMonth(int year, String month) {
        int monthNum = 0;
        switch (month.toLowerCase()) {
            case "jan":
            case "january":
                monthNum = 1;
                break;
            case "feb":
            case "february":
                monthNum = 2;
                break;
            case "mar":
            case "march":
                monthNum = 3;
                break;
            case "apr":
            case "april":
                monthNum = 4;
                break;
            case "may":
                monthNum = 5;
                break;
            case "jun":
            case "june":
                monthNum = 6;
                break;
            case "jul":
            case "july":
                monthNum = 7;
                break;
            case "aug":
            case "august":
                monthNum = 8;
                break;
            case "sep":
            case "september":
                monthNum = 9;
                break;
            case "oct":
            case "october":
                monthNum = 10;
                break;
            case "nov":
            case "november":
                monthNum = 11;
                break;
            case "dec":
            case "december":
                monthNum = 12;
                break;
            default:
                return 0; // invalid month
        }
        return getDaysInMonth(year, monthNum);
    }

    public static int getDaysElapsedInMonth(Date date) {
        int year = date.getYear() + 1900;
        int month = date.getMonth() + 1;
        int dayOfMonth = date.getDate();
        int daysInMonth = getDaysInMonth(year, month);

        return Math.min(dayOfMonth, daysInMonth);
    }

    public static int getDaysElapsedInMonth(Date date, int year1, int month1) {
        int year = date.getYear() + 1900;
        int month = date.getMonth() + 1;
        int dayOfMonth = date.getDate();
        int daysInMonth = getDaysInMonth(year, month);

        if (year < year1 || (year == year1 && month < month1)) {
            return 0;
        } else if (year == year1 && month == month1) {
            return Math.min(dayOfMonth, daysInMonth);
        } else {
            return getDaysInMonth(year1, month1);
        }
    }

    public static String convertToYYMM(int year, String month) {
        String yy = Integer.toString(year % 100);  // Extract the last two digits of the year

        // Map the month abbreviation to its numeric representation
        String mm;
        switch (month.toUpperCase()) {
            case "JAN":
                mm = "01";
                break;
            case "FEB":
                mm = "02";
                break;
            case "MAR":
                mm = "03";
                break;
            case "APR":
                mm = "04";
                break;
            case "MAY":
                mm = "05";
                break;
            case "JUN":
                mm = "06";
                break;
            case "JUL":
                mm = "07";
                break;
            case "AUG":
                mm = "08";
                break;
            case "SEP":
                mm = "09";
                break;
            case "OCT":
                mm = "10";
                break;
            case "NOV":
                mm = "11";
                break;
            case "DEC":
                mm = "12";
                break;
            default:
                throw new IllegalArgumentException("Invalid month abbreviation: " + month);
        }

        // Combine the year and month in YYMM format
        return yy + mm;
    }

    public static Date getFirstDateOfMonth(Date date) throws Exception {

        if(date==null){
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.DAY_OF_MONTH, 1);
        date = c.getTime();

        date = DateUtil.removeTimestampFromDate(date);

        return date;

    }

    public static Date removeTimestampFromDate(Date date) throws Exception {

        if(date==null){
            return null;
        }

        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");

        String dateStr = outputFormat.format(date);

        return inputFormat.parse(dateStr);

    }

    public static Date getLastDateOfMonth(Date date) throws Exception{


        DateTime dateTime = new DateTime(date);

        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();

        YearMonth yearMonth = YearMonth.of(year, month);

        LocalDate lastDayOfMonth = yearMonth.atEndOfMonth();

        Date lastDateOfMonth = java.sql.Date.valueOf(lastDayOfMonth);

        return lastDateOfMonth;

    }

    public static Month getMonthFromDate(Date date) throws Exception{

      DateTime dateTime = new DateTime(date);

      int mon = dateTime.getMonthOfYear();

      for(Month m : Month.values()){
          if(m.ordinal()+1==mon){
              return m;
          }
      }

      return null;

    }

    public static Date getLastDateOfTheYear(Date date){

        int currentYear = new DateTime(date).getYear();

        // Get the last date of the year
        LocalDate lastDateOfYear = Year.of(currentYear).atDay(1).with(TemporalAdjusters.lastDayOfYear());

        return new DateTime(lastDateOfYear).toDate();

    }

    public static Date constructDate(int day, Month month, int year) throws Exception {

        int monthInt = month.ordinal()+1;

        String monthStr = monthInt < 10 ? "0" + monthInt : monthInt + "";

        String dayStr = day < 10 ? "0" + day : day + "";

        String dateStr = year+"-"+monthStr+"-"+dayStr;

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");

        return inputFormat.parse(dateStr);

    }

    public static Date addYearsToDate(Date startDate, Double years) throws Exception {

        if(years==null){
            years = 0.0;
        }

        DateTime startDt = new DateTime(startDate);

        LocalDate dob = LocalDate.of(startDt.getYear(), startDt.getMonthOfYear(),
                startDt.getDayOfMonth());

        LocalDate ret = dob.plusYears(years.intValue());
        LocalDate lastDay = ret.with(TemporalAdjusters.lastDayOfMonth());
        LocalDate firstDay = ret.with(TemporalAdjusters.firstDayOfMonth());

        if (ret.isAfter(firstDay)){
            ret=lastDay;
        }else{
            ret=ret.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
        }

        Date dateFinal = new SimpleDateFormat("yyyy-MM-dd").parse(ret.toString());

        return dateFinal;

    }

    public static List<Date> getYearStartDates(Date firstYearStartDate, List<Date> otherDates) throws Exception {

        Date runningStart = firstYearStartDate;

        Date maxDate = Collections.max(otherDates);

        List<Date> startingDates = new ArrayList<>();

        while (runningStart.before(maxDate) || runningStart.equals(maxDate)){

            startingDates.add(runningStart);

            runningStart = new DateTime(runningStart).plusYears(1).plusDays(10).toDate();

            runningStart = DateUtil.getFirstDateOfMonth(runningStart);

        }

        return startingDates;

    }

}
