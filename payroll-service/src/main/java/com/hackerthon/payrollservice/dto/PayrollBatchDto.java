package com.hackerthon.payrollservice.dto;

import com.hackerthon.payrollservice.model.enums.Month;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

/**
 * DTO for {@link com.hackerthon.payrollservice.entity.PayrollBatch}
 */
@Value
@Builder
@AllArgsConstructor
public class PayrollBatchDto implements Serializable {
    Long id;

    Month month;

    int year;

    Integer batchCount;

    BigDecimal netTotal;
}