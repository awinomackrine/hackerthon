package com.hackerthon.payrollservice.dto;

import com.hackerthon.payrollservice.model.enums.PayType;
import com.hackerthon.payrollservice.model.enums.PaymentFrequency;
import com.hackerthon.payrollservice.model.enums.PensionerStatus;
import com.hackerthon.payrollservice.model.enums.PensionerType;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PensionerForPayrollDTO {
    private Long id;

    private String pensionerNo;

    private BigDecimal dcPension;

    private BigDecimal dbPension;

    private Long schemeId;

    private PaymentFrequency pensionFrequency;

    private PensionerStatus pensionStatus;

    private PayType payType;

    private PensionerType pensionerType;

    private Date pensionStopDate;

}
