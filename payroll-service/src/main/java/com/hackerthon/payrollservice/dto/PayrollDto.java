package com.hackerthon.payrollservice.dto;

import com.hackerthon.payrollservice.entity.Payroll;
import com.hackerthon.payrollservice.model.enums.Month;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Value;

/**
 * DTO for {@link Payroll}
 */
@Value
public class PayrollDto implements Serializable {
    Long id;

    Month month;

    int year;

    BigDecimal gross;

    BigDecimal net;

    BigDecimal tax;

    Long pensionerId;
}