package com.hackerthon.payrollservice.repository;

import com.hackerthon.payrollservice.entity.Payroll;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayrollRepository extends JpaRepository<Payroll, Long> {

}
