package com.hackerthon.payrollservice.repository;

import com.hackerthon.payrollservice.entity.PayrollBatch;
import com.hackerthon.payrollservice.model.enums.Month;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayrollBatchRepository extends JpaRepository<PayrollBatch, Long> {

    PayrollBatch getPayrollBatchByMonthAndYearAndSchemeId(Month month, int Year, Long schemeId);

    List<PayrollBatch> getAllBySchemeId(Long schemeId);
}
